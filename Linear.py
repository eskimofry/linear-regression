import numpy
import matplotlib.pyplot as plt

class Hypothesis(object):
    def Identity(X):
        return X

    def Linear(thetas, x):
        #print("thetas.shape: {}, x.shape: {}".format(thetas.shape, x.shape))
        return thetas.dot(x)


class LinearRegression(object):
    def __init__(self, hypothesis = Hypothesis.Linear, features=1, alpha=0.05):
        self.m = features
        self.hypothesis = hypothesis
        self.alpha = alpha

    def Fit(self, data: numpy.array):
        if self.m != data.shape[0] - 1:
            print("number of columns of data({}) does not equal number of features expected({})".format(data.shape[0] - 1, self.m))
            raise Exception("Incorrect Data")
        n = data.shape[1]
        X = data[0:self.m]
        y = data[-1]

        self.thetas = numpy.array([-1] * (self.m + 1))
        
        ones = numpy.ones((n, 1), dtype=int)
        X_mod = numpy.concatenate((ones.T,X),0)

        plt.ylabel('y')
        plt.xlabel('x')
        X_a = numpy.asarray(X).flatten()
        y_a = numpy.asarray(y).flatten()
        plt.plot(X_a, y_a, 'b')

        i = 0
        for i in range(1,10000):
            yhat = self.hypothesis(self.thetas, X_mod)

            diff = yhat - y

            cost = sum(diff) ** 2

            print("iter: {}, cost: {}".format(i, cost))

            rhs = diff.dot(X_mod.T)

            self.oldthetas = self.thetas
            self.thetas = self.thetas - self.alpha * (rhs / (2 * self.m))

            if(numpy.any(numpy.absolute(self.thetas - self.oldthetas) < 0.0000000001)):
                break

            y_run = self.hypothesis(self.thetas, X_mod)
            X_a = numpy.asarray(X).flatten()
            y_a = numpy.asarray(y_run).flatten()
            plt.plot(X_a, y_a, 'r')

        print("After {} iterations, thetas are: {}".format(i, self.thetas))
        y_run = self.hypothesis(self.thetas, X_mod)
        X_a = numpy.asarray(X).flatten()
        y_a = numpy.asarray(y_run).flatten()
        plt.plot(X_a, y_a, 'y')
        plt.title("done.. quiting in 5 secs")
        plt.pause(5)


if __name__ == "__main__":
    lr = LinearRegression(hypothesis = Hypothesis.Linear, features = 1)
    data = numpy.array([[1, 2, 3, 4, 5], [ 5, 4, 3, 2, 1]])
    print("Testing LinearRegression with y = mx+c for data:\n {}".format(data))
    lr.Fit(data)

